﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int total_coin = 0; //digunakan untuk mencatat coin
    public Text CoinLabel; //digunakan untuk menampilkan coin

    //dipakai untuk memanggil method class ini dari class lain
    private static GameController instance;

    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Digunakan untuk menambah skor
    public void AddCoin()
    {
        total_coin++;
        CoinLabel.text = "Coin: " + total_coin.ToString();
    }

    public static GameController GetInstance()
    {
        return instance;
    }
}
