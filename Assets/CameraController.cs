﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    // Use this for initialization
    public GameObject Player;
    public float MinX = 0; //Batas kiri kamera
    public float MaxX = 25.6f; //Batas kanan kamera

    float smoothness = 1; //Untuk mengatur agar kamera tidak bergerak terlalu cepat

    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        pos.x = Player.transform.position.x;

        if (pos.x < MinX) pos.x = MinX;
        else if (pos.x > MaxX) pos.x = MaxX;

        transform.position = Vector3.Lerp(transform.position, pos, smoothness * Time.deltaTime);
    }
}
